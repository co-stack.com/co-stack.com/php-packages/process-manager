# co-stack/process-manager

A simple wrapper to enable parallel processing
using [Symfony Process](http://symfony.com/doc/current/components/process.html) component.

## Installation

`composer require co-stack/process-manager`

## Example

```php
<?php

use CoStack\ProcessManager\ProcessManager;
use Symfony\Component\Process\Process;

$proc1 = new Process(['ls', '-l']);
$proc2 = new Process(['ls', '-l']);

$processmanager = new ProcessManager();

$processes = [
    $proc1,
    $proc2,
];

$maxParallelProcesses = 5;
// microseconds
$pollingInterval = 1000;
$processmanager->runParallel($processes, $maxParallelProcesses, $pollingInterval);
```

## Other

This is a fork of [jagandecapri/symfony-parallel-process](https://github.com/jagandecapri/symfony-parallel-process)
where I contributed to in 2016. Since that, the repo was not maintained, so I decided to take over and update it.
